# Projet back symfony 

Prérequis:
Assurer d'avoir installer docker et docker compose sur votre machine.

Je vous conseille d'utiliser votre VM debian pour bien utiliser l'app

Connectez votre compte github avec compte gitlab ou creer un compte gitlab

cloner le repo https://gitlab.com/enock18/arosaje-mspr.git

Pour lancer l'application utiliser la commande `docker-compose up -d`

Pour voir l'application `symfony` aller sur http://localhost:8081 

Pour gérer la base de données `phpmyadmin` http://localhost:8080
pour vous connecter mettez root à l'identifiant et rien pour password ensuite cliquez sur connexion

Pour voir l'application `react` http://localhost:3000



1.Pour utiliser la base de données créer un fichier `.env.local`, copier tout ce qui est  sur `.env` à l'interieur du fichier `.env.local` 

2.Ensuite lancer la commande `docker exec -ti www_arosaje bash` ensuite cd app-back.

`php bin/console doctrine:database:create`